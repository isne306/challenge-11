﻿#include <iostream>
#include "list.h"

using namespace std;

void List::printList()
{
	Node *tmp = head;
	
	while(tmp != NULL)
	{
		cout << tmp->info << " ";
		tmp = tmp->next;
	}
}

List::~List()
{
	for(Node *p; !isEmpty(); )
	{
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int x)
{
	Node *p = new Node(x);

	if(isEmpty())
	{
		head = p;
		tail = p;
	}
	else
	{
		p->next = head;
		head->prev = p;
		p->prev = NULL;
		head = p;
	}

}

void List::tailPush(int x)
{
	Node *p = new Node(x);

	if(isEmpty())
	{
		head = p;
		tail = p;
	}
	else
	{
		tail->next = p;
		p->prev = tail;
		tail = tail->next;

	}
}

int List::headPop()
{
	if(isEmpty())
	{
		cout << "List is Empty!";
		return -1;
	}
	else
	{
		int num;
		Node *tmp = head;

		num = head->info;
		head = head->next;

		head->prev = NULL;
		delete tmp;
		return num;
	}
}

int List::tailPop()
{
	if(isEmpty())
	{
		cout << "List is Empty!";
		return -1;
	}
	else
	{
		if(head==tail)
		{
			int num;
			Node *tmp = head;
			num = head->info;

			delete tmp;

			head = NULL;
			tail = NULL;

			return num;
		}
		else
		{
			int num;
			Node *tmp = tail;

			tmp = tmp->prev;

			num = tail->info;

			delete tail;

			tmp->next = NULL;

			tail = tmp;

			return num;
		}
	}

}

bool List::isInList(int x)
{
	Node *tmp = head;

	while(tmp != NULL)
	{
		if(tmp->info == x )
		{
			return true;
		}
		else
		{
			tmp = tmp->next;
		}
	}
	return false;
}

void List::deleteNode(int x)
{
	Node *tmp = head;
	//Node *del;

	if(isInList(x))
	{
		if(x == head->info)
		{
			headPop();
		}
		else if(x == tail->info)
		{
			tailPop();
		}
		else
		{
			while(tmp != NULL)
			{
				if(tmp->info == x)
				{
					Node *tmp2 = tmp->prev;
					tmp2->next = tmp->next;

					Node *tmp3 = tmp->next;
					tmp3->prev = tmp->prev;

					delete tmp;
					break;
				}
				tmp = tmp->next;
			}
		}
	}
	else
	{
		cout << "NOT FOUND";
	}
}
		