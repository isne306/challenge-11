﻿#include <iostream>
#include "list.h"

using namespace std;

int main()
{
	List b;

	b.headPush(1);
	b.headPush(2);
	b.headPush(3);
	b.printList();

	cout << endl;

	b.tailPush(4);
	b.tailPush(5);
	b.tailPush(6);
	b.printList();

	cout << endl;

	cout<< "Pop from head: " << b.headPop() << endl;

	b.printList();

	cout << endl;

	cout<< "Pop from tail: " << b.tailPop() << endl;

	b.printList();

	cout << endl;

	if(b.isInList(4))
	{
     	cout << "Found 4"<< endl;
	}
	if(b.isInList(9))
	{
     	cout << "Found 9" << endl;
	}
	if(b.isInList(7))
	{
     	cout << "Found 7" << endl;
	}
	if(b.isInList(5))
	{
     	cout << "Found 5" << endl;
	}
	if(b.isInList(1))
	{
     	cout << "Found 1" << endl;
	}

	b.printList(); // 2 1 4 5

	cout << endl;
	b.deleteNode(1); // *****
	cout << endl;

	b.printList();

	cout << endl;
	b.deleteNode(2);
	cout << endl;

	b.printList();

	cout << endl;
	b.deleteNode(5);
	cout << endl;

	b.printList();

}